<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleStoreRequest;
use App\Models\Article;
use App\Traits\ResponseWorks;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    use ResponseWorks;

    public function store(ArticleStoreRequest $request)
    {
        $data = $request->validated();
        $article = Article::create($data);
        $article->tags()->sync($data['ids']);

        return self::response($article->load('tags'), 'The article created successfully!');
    }
}
