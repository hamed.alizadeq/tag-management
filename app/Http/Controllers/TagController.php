<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagStoreRequest;
use App\Http\Requests\TagUpdateRequest;
use App\Http\Resources\TagResource;
use App\Models\Tag;
use App\Traits\ResponseWorks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TagController extends Controller
{
    use ResponseWorks;

    public function index(request $request)
    {
        return response(TagResource::paginate(15));
    }

    public function show(Tag $tag)
    {
        return self::response(new TagResource($tag));
    }

    public function store(TagStoreRequest $request)
    {
        $image = $request->file('image');
        if ($request->filled('image')) {
            $imagePath = $image->storeAs('tags', 'tag-' . uniqid() . '.' . $image->getClientOriginalExtension());
        }
        $tag = Tag::create([
            'title' => $request->title,
            'slug' => $request->slug,
            'description' => $request->description,
            'image_path' => $imagePath ?? null
        ]);
        return self::response($tag, 'The tag stored successfully.');
    }

    public function update(Tag $tag, TagUpdateRequest $request)
    {
        $isFilledImage = ! is_null($request->file('image'));
        $isFilledImagePath = ! is_null($tag->image_path);
        $isExistImage = Storage::exists($tag->image_path);

        if ($isFilledImage && $isFilledImagePath && $isExistImage) {
            Storage::delete($tag->image_path);
        }

        $image = $request->file('image');
        if ($isFilledImage) {
            $imagePath = $image->storeAs('tags', 'tag-' . uniqid() . '.' . $image->getClientOriginalExtension());
        }

        $data = array_merge($request->validated(), ( ! empty($imagePath) ? ['image_path' => $imagePath] : []));
        $tag->update($data);
        return self::response($tag, 'The tag updated successfully.');
    }

    public function delete(Tag $tag)
    {
        if ( ! empty($tag->image_path) && Storage::exists($tag->image_path)) { Storage::delete($tag->image_path); }

        $tag->delete();
        return self::response(null, 'The tag deleted successfully!');
    }
}
