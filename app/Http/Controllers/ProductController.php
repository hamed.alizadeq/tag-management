<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Models\Product;
use App\Traits\ResponseWorks;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use ResponseWorks;

    public function store(ProductStoreRequest $request)
    {
        $data = $request->validated();
        $product = Product::create($data);
        $product->tags()->sync($data['ids']);

        return self::response($product->load('tags'), 'The product created successfully!');
    }
}
