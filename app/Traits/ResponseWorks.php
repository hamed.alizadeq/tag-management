<?php


namespace App\Traits;


trait ResponseWorks
{
    public static function response($data, $message = '', int $code = 200) : array
    {
        return [
            'data' => $data ?? null,
            'message' => $message ?? '',
        ];
    }
}
